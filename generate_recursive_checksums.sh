#!/bin/bash
# Handle input
while getopts "sv" flag; do
case "$flag" in
    s) shallow=true;;
    v) verbose=true;;
    #-depth
esac
done

if [ $(( $# - $OPTIND )) -lt 0 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

if [ $(( $# - $OPTIND )) -gt 0 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

file1=${@:$OPTIND:1}
# Remove trailing slashes, either:
#shopt -s extglob
#[[ file1 = *[!/] ]] || file1=${file1%%*(/)}
# or:
case $file1 in
  *[!/]*/) file1=${file1%"${file1##*[!/]}"};;
  *[/]) file1="/";;
esac

# Now replace spaces by escaped spaces
#file1="${file1// /\\ }"
HASHING_GENERATOR=md5sum
# Current folder . should be expanded in the _result name_
result_file_suffix=.md5
if  [ "$file1" = "." ]; then
  result_file_name="$(realpath . --relative-to=..)$result_file_suffix"
else
  result_file_name="$file1$result_file_suffix"
fi

if [ "$verbose" = true ]; then
    echo "Should be filename: $0";
    echo "Should be foldername: $file1";
    echo "Should be towards: $result_file_name";
    echo "Shallow is: '$shallow'";
fi

LINKFLAG="-L"
DEPTHFLAGS="-maxdepth 1"
DIRFLAGS="-type d ! -empty"
FILEFLAGS="-type f"

CHECKSUM_SCRIPT="./generate_checksums.sh"
SUMMARY_FILE="summary$result_file_suffix"
# Find, while, and read as described http://mywiki.wooledge.org/BashFAQ/001 and https://askubuntu.com/a/343753
#find . -type f -name '*.*' -print0 |
find $LINKFLAG $file1 $DEPTHFLAGS $DIRFLAGS ! -path $file1 -print0 |
while IFS= read -r -d '' file; do
    printf 'Checksumming %s recursively\n' "$file"
    $CHECKSUM_SCRIPT "$file"
done
md5sum *.md5 > $SUMMARY_FILE
