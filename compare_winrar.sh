#!/bin/bash
# Shift construct
# Handle input
ii=1;
jj=$#;
while [ $ii -le $jj ]; do
    echo "Arg - $ii: $1";
    if [ $ii == 1 ]; then
        file1=$1
    elif [ $ii == 2 ]; then
        file2=$1
    fi
    ii=$((ii + 1));
    shift 1;
done

echo "Should be filename: $0";
echo "Should be empty: $1";
echo "Should be file1: $file1";
echo "Should be file2: $file2";

tmp1=$(mktemp -d "/tmp/$file1.tmp.XXX")
tmp2=$(mktemp -d "/tmp/$file2.tmp.XXX")

# Example:
# unrar x Tyrant_Workspace_20240325121818.rar /tmp/Tyrant_Workspace_20240325121818.rar.tmpnir/
# Will create a folder Tyrant_Workspace in rar /tmp/Tyrant_Workspace_20240325121818.rar.tmpnir/
# We use this to unpack
UNRAR=unrar
echo "Unrarring using $UNRAR"
echo "Starting with unrarring $file1 to $tmp1"
$UNRAR x "$file1" "$tmp1" > /dev/null
UNRAR1_STATUS=$?
echo "Done with status=$UNRAR1_STATUS! Starting with unrarring $file2 to $tmp2"
$UNRAR x "$file2" "$tmp2" > /dev/null
UNRAR2_STATUS=$?

DIFF=diff
DIFF_FLAGS=-qr
echo "Done with status=$UNRAR2_STATUS! Generating diff"
$DIFF $DIFF_FLAGS $tmp1 $tmp2
DIFF_STATUS=$?
echo "Done! Diff status = $DIFF_STATUS. 0=no diff"
if [[ $tmp1 == /tmp* ]] && [[ $tmp2 == /tmp* ]]; then
    echo "rm rfing temp folders"
    rm -rf $tmp1 $tmp2
fi
