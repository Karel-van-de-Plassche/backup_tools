#!/bin/bash
# This script runs the "Karel-style" backup using rsync.
# Generally, there are 3 locations:
# 1. The file/folder being back-upped
# 2. The "local disk" back-up store
# 3. The "on-site disk" back-up store
# Optionally one can add
# 4. The "off-site server disk" back-up store
# This means we always have at least 3 copies of the same file
# We KISS, so we make a standard master-slave chain as to
# prove who has the "real" file. This is simply 1 -> 2 -> 3 -> 4
# We are aware that this will not protect us against propagating bit
# rot, nor delayed crypto attacks. We naively assume that's somebody
# else's problem. We are only storing "Losable" files here!
# Also, we only do 2-way backups, never 3-way. Again, dangerous, but screw it
# Finally, we use --delete. NEVER TRY TO FLIP A SRC AND A DEST!! Data loss _will_ occur
#
# We assume our disk is a WSL mounted NTFS disk on Windows, or a NTFS-3g mounted NTFS disk on Linux
# 
# Our QuaLiKiz default flags are "-avHAXSP --partial"
#
# For reference, find the Rsync flags here:
# --archive, -a            archive mode is -rlptgoD (no -A,-X,-U,-N,-H)
# --verbose, -v            increase verbosity
# --hard-links, -H         preserve hard links
# --acls, -A               preserve ACLs (implies --perms)
# --perms, -p              preserve permissions
# --xattrs, -X             preserve extended attributes
# --sparse, -S             turn sequences of nulls into sparse blocks
# -P                       same as --partial --progress
# --partial                keep partially transferred files
# --progress               show progress during transfer
#
# We add
# --delete                 delete extraneous files from dest dirs
# --recursive, -r          recurse into directories
# recursive is needed because we filter using --files-from in the root
# --files-from=FILE        read list of source-file names from FILE
# to let Rsync do what we want
#
# And remove
# --omit-dir-times, -O     omit directories from --times

# Parse arguments
while getopts "v" flag; do
  case "$flag" in
    v) verbose=true;;
  esac
done
mode=$1
if [ $(( $# - $OPTIND )) -lt 0 ]; then
  echo "Usage: `basename $0` [options] ARG1"
  exit 1
fi

if [ $(( $# - $OPTIND )) -gt 0 ]; then
  echo "Usage: `basename $0` [options] ARG1"
  exit 1
fi

mode=${@:$OPTIND:1}

# Now we assume that the Windows and Linux usernames match up. Meaning e.g. windows=>Karel, linux=>karel
# we also define src and dest, as defined in the Rsync man pages
luser=karel
wuser=${luser^}
userroot="/mnt/c/Users/$wuser/"

# Set up common rsync bits and pieces
logname="rsync_logs/tier${mode}_$(date +%Y%m%d_%H%M%S).log"
if [ $mode -eq 0 ]; then
  echo "Running tier $mode backup, live -> local disk '$file_list'"
  src=$userroot
  dest=/mnt/f/backup/
  filterfile="merge rsync_live_backup_filter.txt"
elif [ $mode -eq 1 ]; then
  echo "Running tier $mode backup, local disk -> on-site disk '$file_list'"
  src="/mnt/f/backup/"
  dest="rbpi-red:/mnt/data/backup/"
  filterfile="merge rsync_remote_backup_filter.txt"
elif [ $mode -eq 2 ]; then
  echo "Running tier $mode backup, local disk -> on-site disk '$file_list'"
  src="/mnt/f/backup/"
  dest="rbpi-blue:/mnt/data/backup/"
  filterfile="merge rsync_remote_backup_filter.txt"
elif [ $mode -eq 3 ]; then
  echo "Running alternative tier 1 backup, local disk -> second local disk '$file_list'"
  src="/mnt/f/backup/"
  dest="/mnt/i/backup/"
  filterfile="merge rsync_remote_backup_filter.txt"
elif [ $mode -eq 4 ]; then
  echo "Running alternative tier 2 backup, local disk -> second local disk '$file_list'"
  src="/mnt/f/backup/"
  dest="/mnt/g/backup/"
  filterfile="merge rsync_remote_backup_filter.txt"
else
  echo "Please select mode"
fi
#P --info=progress2
rsync_flags=(-avHAXS --partial --delete -r --log-file=$logname --filter="""$filterfile""")

if [ "$verbose" = true ]; then
  echo "Windows user: '$wuser'";
  echo "userroot: '$userroot'";
  echo "Flags: '$rsync_flags'";
  echo "Root: '$userroot'";
  echo "Source: '$src'";
  echo "Destination: '$dest'";
fi

set -x
rsync "${rsync_flags[@]}" $src $dest
set +x
echo "Tier $mode backup done!"

# old
#remote_user=karel
#remote_ip=192.168.1.100
#remote_port=22
#remote_root="/media/toshiba/backup"
