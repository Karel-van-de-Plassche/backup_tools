#!/bin/bash
# Handle input
if [ $(( $# - $OPTIND )) -lt -1 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

if [ $(( $# - $OPTIND )) -gt -1 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

md5files="*.md5"
#md5sum --check --quiet --strict $md5files
for file in $md5files; do
    echo "Start checking $file"
    md5sum --check --strict --quiet "$file"; result=$?
    case $result in
    0) echo "$file matches passed the sumcheck. Done!" >&2;;
    *) echo "$file does not match! Check folders and md5 file" >&2;;
    esac
done
