#!/bin/bash
# Handle input
while getopts "s" flag; do
case "$flag" in
    s) shallow=true;;
    #-depth
esac
done

if [ $(( $# - $OPTIND )) -lt 0 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

if [ $(( $# - $OPTIND )) -gt 0 ]; then
    echo "Usage: `basename $0` [options] ARG1"
    exit 1
fi

file1=${@:$OPTIND:1}
# Remove trailing slashes, either:
#shopt -s extglob
#[[ file1 = *[!/] ]] || file1=${file1%%*(/)}
# or:
case $file1 in
  *[!/]*/) file1=${file1%"${file1##*[!/]}"};;
  *[/]) file1="/";;
esac

# Now replace spaces by escaped spaces
#file1="${file1// /\\ }"
HASHING_GENERATOR=md5sum
# Current folder . should be expanded in the _result name_
result_file_suffix=.md5
if  [ "$file1" = "." ]; then
  result_file_name="$(realpath . --relative-to=..)$result_file_suffix"
else
  result_file_name="$file1$result_file_suffix"
fi

if [ "$verbose" = true ]; then
    echo "Should be filename: $0";
    echo "Should be foldername: $file1";
    echo "Should be towards: $result_file_name";
    echo "Shallow is: '$shallow'";
fi

LINKFLAG="-L"
DEPTHFLAGS="-maxdepth 1"
DIRFLAGS="-type d ! -empty"
FILEFLAGS="-type f"
if [ "$shallow" = true ]; then
  echo "Generating shallowly into '$result_file_name'"
  find $LINKFLAG "$file1" $DEPTHFLAGS $FILEFLAGS ! -path $file1 -exec $HASHING_GENERATOR {} \; > "$result_file_name"
  if [ -n "$(find $LINKFLAG "$file1" $DEPTHFLAGS ! -path $file1 $DIRFLAGS 2>/dev/null)" ]; then
      echo "Warning! Generating shallow MD5sum, but '$file1' contains non-empty folders! Run non-shallow, or run this script once per folder!"
  else
      echo "Done!"
  fi
else
  echo "Generating deeply into $result_file_name"
  find $LINKFLAG "$file1" $FILEFLAGS -exec $HASHING_GENERATOR {} \; > "$result_file_name"
fi
